#!/usr/bin/env python
# coding: utf-8

# Classification ML
# ==
# The critical features used in this transation predictions are:
# 
# `['user_id', 'uid_y', 'browsing_id','SS_T_First', 'SS_T_Last','SS_DL_T_First','SS_DL_T_Last', 'SS_Page_Count_Sum','SS_Page_Count_Mean', 'SS_Page_Count_Std','SS_T_Sum_Sum', 'SS_T_Sum_Std', 'SS_T_Mean_Sum', 'SS_T_Mean_Std', 'SS_T_Std_Sum', 'SS_T_Std_Std', 'SS_DL_Count_Sum', 'SS_T_Sum_Mean', 'SS_T_Mean_Mean', 'SS_T_Std_Mean', 'SS_VL_Count_Sum', 'SS_VL_Count_Mean', 'SS_VL_Count_Std', 'SS_VL_T_Sum_Sum', 'SS_VL_T_Sum_Mean', 'SS_VL_T_Sum_Std', 'SS_VL_T_Mean_Sum', 'SS_VL_T_Mean_Mean', 'SS_VL_T_Mean_Std', 'SS_VL_T_Std_Sum', 'SS_VL_T_Std_Mean', 'SS_VL_T_Std_Std', 'SS_VL_T_First', 'SS_VL_T_Last', 'view_diff','browse_diff', 'first_last_diff']`
#  
# 
# ### This notebook acts to automate the machine learning training step of dataset.

# In[1]:


# loading all the library

import pandas as pd
import numpy as np
from joblib import dump, load

seed = 18 # seed for random generator


# In[2]:


def load_file(trans):
    """
    Receives the .csv files names to load (or location/path to the .csv file). Uses pandas to 
    read the .csv file to generate the dataframe of the datafile.
    
    
    Parameters
    ----------
    trans : str
        The name of the data file (must be a .csv file) that has all the transaction data. 
        If the data file is in the same location as this script file, just input the file name 
        directly using "file_name" method. Else, give the path to the file.
        
    
    Return
    ------
    trans_data : dataframe object (pandas)  
    
    
    """

    trans_data = pd.read_csv(trans)

    return(trans_data)


# In[3]:


def load_dataframe(file_1):
    """
    This function take the loaded pandas dataframes and processes data. The dataframes 
    is further processed using the following:
    
    1. Takes only the data where total browsing time for a single browsing_id is less than 10900s.
    2. Removes all the data where the total browsing time is less than 0 seconds.
    3. Selects only the listed features.
    4. Separates the X (feature parameters) by taking the relevant features only. 
    
    Step 1 and 2 above is to remove any outlier in the data.
    
    Parameters
    ----------
    file_1 : dataframe object (pandas)
        Dataframe that is loaded from the data file. The file must be preprocessed using KJ's
        algorithm first and then Rish's algorithm.
  
    
    Return
    ------    
    X : dataframe object (pandas)
        X (feature parameters) which are the training/test features.
    
    user : dataframe object (pandas)
        List of users based their user_id.
    
    """
    
    seed = 18
    class_data = file_1.copy()
    print("class_data.shape", class_data.shape)
    print("class_data.user_id.nunique() = ",class_data.user_id.nunique())
    class_data_1 = class_data[class_data.SS_T_Sum_Sum < 10900].copy()
    class_data_1.reset_index(inplace=True)
    class_data_1.drop(['index'], axis=1, inplace=True)
    class_data_1 = class_data_1[~(class_data_1.SS_T_Sum_Sum < 0)].copy()
    class_data_1.reset_index(inplace=True)
    class_data_1.drop(['index'], axis=1, inplace=True)
    print("class_data_1.shape = ", class_data_1.shape)
    
    X = class_data_1.loc[:,['user_id','uid',                            'SS_Page_Count_Sum', 'SS_Page_Count_Mean','SS_Page_Count_Std',                            'SS_T_Sum_Sum', 'SS_T_Sum_Mean', 'SS_T_Sum_Std',                             'SS_T_Mean_Sum', 'SS_T_Mean_Mean', 'SS_T_Mean_Std',                             'SS_T_Std_Sum', 'SS_T_Std_Mean', 'SS_T_Std_Std',                             'SS_DL_Count_Sum',                            'SS_VL_Count_Sum', 'SS_VL_Count_Mean', 'SS_VL_Count_Std',                            'SS_VL_T_Sum_Sum', 'SS_VL_T_Sum_Mean', 'SS_VL_T_Sum_Std',                            'SS_VL_T_Mean_Sum', 'SS_VL_T_Mean_Mean', 'SS_VL_T_Mean_Std',                            'SS_VL_T_Std_Sum', 'SS_VL_T_Std_Mean', 'SS_VL_T_Std_Std',                            'VL_time_diff', 'browse_diff', 'first_last_diff']]
    
    user = class_data_1.loc[:,['user_id','uid']].copy()
    
    return(X,user)
    


# In[4]:


def predict_results(clf, X_test):
    """
    This function take the trained classifier model and the X features to predict the possible 
    outcome for the features (generate the y) i.e. predict which users are mostly to transact
    in the nearest future.
    
    
    Parameters
    ----------
    clf : classifier
        The trained machine learning model from classifier_training() function.
    
    X_test : dataframe object (pandas) or array values
        The input features.  
    
    
    Return
    ------    
    y_pred_class : predicted classes
        The predicted classes by the machine learning model. Usually 1 or 0.
        
    y_pred_proba : predicted probablities values
        The predicted probability of being class 1.
    
    """
    # class predictions and predicted probabilities
    seed = 18
    y_pred_class = clf.predict(X_test.values)
    y_pred_proba = clf.predict_proba(X_test.values)
    return(y_pred_class, y_pred_proba)


# In[20]:


def output_file(user_data, y_class, y_proba):
    """
    This function focuses on user_id and uid, extracted or taken out from the search_data. 
    This user information is combined with the predicted churn class and the predicted probability
    for a user most likely to make a purchase. In all, this function produces the hotlist.csv 
    file which lists users are mostly to make their first (or next) transaction.
    
    Take note that `the output_file` function MUST be placed in the same filepath as the 
    `predict_results` and  `search_model_2019.joblib`.
    
    Parameters
    ----------
    user_data : dataframe object (pandas)
        user_id and uid, extracted or taken out from the search_data. Make sure that the values 
        are not shuffled i.e. the index must not change from the original.
    
    y_pred_class : dataframe object (pandas)
        The predicted classes by the machine learning model., usually 1 or 0.
        
    y_pred_proba : dataframe object (pandas) 
        The predicted probability of being class 1, usually in the range of 1 to 0. 1 means 
        highest probability to churn and 0 means no chance to churn i.e. perfect retention.
    
    
    Return
    ------    
    user_data_group : dataframe object (pandas)
        Returns the merged output of all the parameters. Before that, the output is saved as a 
        file.csv in s3, specifically in s3a://logo-generator-kj/rish/. 
        This path can be changed to suit your requirements.
    
    """

    user_data = pd.concat([user_data,pd.Series(y_class)], axis=1, ignore_index=True)
    user_data = pd.concat([user_data,pd.DataFrame(y_proba)], axis=1, ignore_index=True)

    user_data.rename(columns={0 : 'user_id', 1 : 'uid', 2 : 'class', 3 : 'proba_0', 4 : 'proba_1'}, inplace=True)
    user_data_group = user_data.groupby(['user_id','uid']).mean().reset_index()
    user_data_group.to_csv("s3a://logo-generator-kj/rish/churn_hotlist.csv", index=False)
    return(user_data_group)


# #### What is happening?
# 
# The preprocessed data file (after KJ's algo and Rish's algo) is used in this. After the final preprocessing above, the data is loaded into the ML model developed previously and then the prediction is saved into the s3.

# Predicting
# ==

# In[6]:


# s3a://logo-generator-kj/rish/jun2019_w1_final.csv
file_name = str(input("Enter the file path in s3: "))


# In[7]:


weekly_data = load_file(file_name) 


# In[8]:


(X,user_data) = load_dataframe(weekly_data)


# In[ ]:





# In[9]:


eclf = load("search_model_2019.joblib")


# In[21]:


user_predicted = output_file(user_data, eclf.predict(X.iloc[:, 2::].values), eclf.predict_proba(X.iloc[:, 2::].values))


# In[22]:


user_predicted[user_predicted.proba_1 > 0.6].shape


# In[23]:


user_predicted.head()


# In[ ]:




