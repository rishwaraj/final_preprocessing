#!/usr/bin/env python
# coding: utf-8

# Classification ML - Training the model
# ==
# It is found that the features related to the download i.e. `SS_DL_xx` series, tend to make the model overfit. Removing these features gives us a much better overview of the data. Therefore, these are these features are identified as core features:
# 
# `'SS_Page_Count_Sum', 'SS_Page_Count_Mean', 'SS_Page_Count_Std',
#  'SS_T_Sum_Sum', 'SS_T_Sum_Mean', 'SS_T_Sum_Std', 
#  'SS_T_Mean_Sum','SS_T_Mean_Mean', 'SS_T_Mean_Std', 
#  'SS_T_Std_Sum', 'SS_T_Std_Mean', 'SS_T_Std_Std', 'browsing_diff', 'first_last_diff'`
#  
# This .py script file requires the trained machine learning model 'model_2019.joblib' for a
# full execution. Please make sure that this .py script and the 'model_2019.joblib' file are
# both in the same working directory. Failing that, please make sure that the path to the
# machine learning model is given during the eclf = load() command.
# 
# ### This notebook acts to automate the machine learning training step of dataset.

# In[1]:


# loading all the library

import pandas as pd
import numpy as np
from joblib import dump, load

seed = 18 # seed for random generator


# In[2]:


def load_file(trans):
    """
    Receives the .csv files names to load (or location/path to the .csv file). Uses pandas to 
    read the .csv file to generate the dataframe of the datafile.
    
    
    Parameters
    ----------
    trans : str
        The name of the data file (must be a .csv file) that has all the transaction data. 
        If the data file is in the same location as this script file, just input the file name 
        directly using "file_name" method. Else, give the path to the file.
        
    
    Return
    ------
    trans_data : dataframe object (pandas)  
    
    
    """

    trans_data = pd.read_csv(trans)

    return(trans_data)


# In[3]:


def load_dataframe(file_1):
    """
    This function take the loaded pandas dataframes and processes data. The dataframes 
    is further processed using the following:
    
    1. Takes only the data where total browsing time for a single browsing_id is less than 10900s.
    2. Removes all the data where the total browsing time is less than 0 seconds.
    3. Selects only the listed features.
    4. Separates the X (feature parameters) by taking the relevant features only. 
    
    Step 1 and 2 above is to remove any outlier in the data.
    
    Parameters
    ----------
    file_1 : dataframe object (pandas)
        Dataframe that is loaded from the data file. The file must be preprocessed using KJ's
        algorithm first and then Rish's algorithm.
  
    
    Return
    ------    
    X : dataframe object (pandas)
        X (feature parameters) which are the training/test features.
    
    user : dataframe object (pandas)
        List of users based their user_id.
    
    """
    
    seed = 18
    class_data = file_1.copy()
    print("class_data.shape", class_data.shape)
    print("class_data.user_id.nunique() = ",class_data.user_id.nunique())
    class_data_1 = class_data[class_data.SS_T_Sum_Sum < 10900].copy()
    class_data_1.reset_index(inplace=True)
    class_data_1.drop(['index'], axis=1, inplace=True)
    class_data_1 = class_data_1[~(class_data_1.SS_T_Sum_Sum < 0)].copy()
    class_data_1.reset_index(inplace=True)
    class_data_1.drop(['index'], axis=1, inplace=True)
    print("class_data_1.shape = ", class_data_1.shape)
    X = class_data_1.loc[:,['SS_Page_Count_Sum', 'SS_Page_Count_Mean','SS_Page_Count_Std',                            'SS_T_Sum_Sum', 'SS_T_Sum_Mean', 'SS_T_Sum_Std',                             'SS_T_Mean_Sum', 'SS_T_Mean_Mean', 'SS_T_Mean_Std', 'SS_T_Std_Sum',                             'SS_T_Std_Mean', 'SS_T_Std_Std', 'browse_diff', 'first_last_diff',]]
    user = class_data_1.loc[:,['user_id']].copy()
    
    return(X,user)
    


# In[10]:


def predict_results(clf, X_test):
    """
    This function take the trained classifier model and the X features to predict the possible 
    outcome for the features (generate the y) i.e. predict which users are mostly to transact
    in the nearest future.
    
    
    Parameters
    ----------
    clf : classifier
        The trained machine learning model from classifier_training() function.
    
    X_test : dataframe object (pandas) or array values
        The input features.  
    
    
    Return
    ------    
    y_pred_class : predicted classes
        The predicted classes by the machine learning model. Usually 1 or 0.
        
    y_pred_proba : predicted probablities values
        The predicted probability of being class 1.
    
    """
    # class predictions and predicted probabilities
    seed = 18
    y_pred_class = clf.predict(X_test.values)
    y_pred_proba = clf.predict_proba(X_test.values)
    return(y_pred_class, y_pred_proba)


# #### What is happening?
# 
# The preprocessed data file (after KJ's algo and Rish's algo) is used in this. After the final preprocessing above, the data is loaded into the ML model developed previously and then the prediction is saved into the s3.

# Predicting
# ==

# In[5]:


# s3a://logo-generator-kj/rish/jun2019_w1_final.csv
file_name = str(input("Enter the file path in s3: "))


# In[6]:


weekly_data = load_file(file_name) 


# In[7]:


(X,user_data) = load_dataframe(weekly_data)


# In[8]:


eclf = load("model_2019.joblib")


# In[11]:


(y_class,y_proba) = predict_results(eclf,X)


# In[19]:


user_data = pd.concat([user_data,pd.Series(y_class)], axis=1, ignore_index=True)
user_data = pd.concat([user_data,pd.DataFrame(y_proba)], axis=1, ignore_index=True)


# In[27]:


user_data.rename(columns={0 : 'user_id', 1 : 'class', 2 : 'proba_0', 3 : 'proba_1'}, inplace=True)


# In[ ]:


user_data.to_csv("s3a://logo-generator-kj/rish/user_convertion_hotlist.csv", index=False)
user_data_group = user_data.groupby(['user_id']).mean()
user_data_group.to_csv("s3a://logo-generator-kj/rish/summary_user_convertion_hotlist.csv", index=False)

