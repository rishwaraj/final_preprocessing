#!/usr/bin/env python
# coding: utf-8

# Classification ML - Training the model
# ==
# It is found that the features related to the download i.e. `SS_DL_xx` series, tend to make the model overfit. Removing these features gives us a much better overview of the data. Therefore, these are these features are identified as core features:
# 
# `'SS_Page_Count_Sum', 'SS_Page_Count_Mean', 'SS_Page_Count_Std',
#  'SS_T_Sum_Sum', 'SS_T_Sum_Mean', 'SS_T_Sum_Std', 
#  'SS_T_Mean_Sum','SS_T_Mean_Mean', 'SS_T_Mean_Std', 
#  'SS_T_Std_Sum', 'SS_T_Std_Mean', 'SS_T_Std_Std', 'browsing_diff', 'first_last_diff'`
#  
# 
# ### This notebook acts to automate the machine learning training step of dataset.

# In[1]:


# loading all the library

import pandas as pd
import numpy as np

from sklearn.model_selection import train_test_split
from sklearn.model_selection import cross_val_score, cross_val_predict
from sklearn.model_selection import GridSearchCV

from sklearn.linear_model import LogisticRegression
from sklearn.naive_bayes import MultinomialNB
from sklearn.naive_bayes import GaussianNB
from sklearn.tree import DecisionTreeClassifier
from mlxtend.classifier import EnsembleVoteClassifier

from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import ExtraTreesClassifier
from sklearn.ensemble import VotingClassifier
from sklearn.ensemble import AdaBoostClassifier
from sklearn.ensemble import GradientBoostingClassifier

from sklearn.metrics import precision_score, recall_score
from sklearn.metrics import accuracy_score
from sklearn.metrics import f1_score
from sklearn.metrics import confusion_matrix
from sklearn.metrics import classification_report
from sklearn.metrics import precision_recall_fscore_support

import xgboost as xgb
from collections import Counter
from imblearn.under_sampling import RandomUnderSampler
from imblearn.over_sampling import SMOTE, ADASYN

get_ipython().run_line_magic('matplotlib', 'inline')

seed = 18 # seed for random generator


# In[2]:


def load_datafile(trans, no_trans):
    """
    Receives the .csv files names to load (or location/path to the .csv file). Uses pandas to read the .csv
    file to generate the dataframe of the datafile.
    
    
    Parameters
    ----------
    trans : str
        The name of the data file (must be a .csv file) that has all the transaction data. If the data file 
        is in the same location as this script file, just input the file name directly using "file_name" method. 
        Else, give the path to the file.
    
    no_trans : str
        The name of the data file (must be a .csv file) that has all the no_transaction data. If the data file 
        is in the same location as this script file, just input the file name directly using "file_name" method. 
        Else, give the path to the file.
        
    
    Return
    ------
    trans_data : dataframe object (pandas)  
    
    notrans_final : dataframe object (pandas)
    
    
    """

    trans_data = pd.read_csv(trans)
    notrans_final = pd.read_csv(no_trans)
    return(trans_data, notrans_final)


# In[41]:


def load_training_data(file_1, file_no):
    """
    This function take the pandas dataframes for the transaction data (file_1) and no_transaction data (file_no).
    Both of these dataframes are concatenated to create a single dataframe and does the following processing:
    
    1. Takes only the data where total browsing time for a single browsing_id is less than 10900 seconds.
    2. Removes all the data where the total browsing time is less than 0 seconds.
    3. Selects only the listed features.
    4. Separates the X (feature parameters) and y (target parameter). 
    
    Step 1 and 2 above is to remove any outlier in the data.
    
    Parameters
    ----------
    file_1 : dataframe object (pandas)
        Dataframe that is loaded from the transaction data file. The file must be preprocessed using KJ's
        algorithm first and then Rish's algorithm.
    
    file_0 : dataframe object (pandas)
        Dataframe that is loaded from the no_transaction data file. The file must be preprocessed using KJ's
        algorithm first and then Rish's algorithm.    
    
    Return
    ------    
    X : dataframe object (pandas)
        X (feature parameters) which are the training/test features.
        
    y : dataframe object (pandas)
        y (target parameter) which are the target output.
    
    """
    
    seed = 18
    class_data = pd.concat([file_1,file_no], ignore_index=True, sort=False)
    print("class_data.shape", class_data.shape)
    print("class_data.user_id.nunique() = ",class_data.user_id.nunique())
    class_data_1 = class_data[class_data.SS_T_Sum_Sum < 10900].copy()
    class_data_1.reset_index(inplace=True)
    class_data_1.drop(['index'], axis=1, inplace=True)
    class_data_1 = class_data_1[~(class_data_1.SS_T_Sum_Sum < 0)].copy()
    class_data_1.reset_index(inplace=True)
    class_data_1.drop(['index'], axis=1, inplace=True)
    print("class_data_1.shape = ", class_data_1.shape)
    X = class_data_1.loc[:,['user_id',                            'SS_Page_Count_Sum', 'SS_Page_Count_Mean','SS_Page_Count_Std',                            'SS_T_Sum_Sum', 'SS_T_Sum_Mean', 'SS_T_Sum_Std',                             'SS_T_Mean_Sum', 'SS_T_Mean_Mean', 'SS_T_Mean_Std',                             'SS_T_Std_Sum', 'SS_T_Std_Mean', 'SS_T_Std_Std',                             'SS_DL_Count_Sum',                            'SS_VL_Count_Sum', 'SS_VL_Count_Mean', 'SS_VL_Count_Std',                            'SS_VL_T_Sum_Sum', 'SS_VL_T_Sum_Mean', 'SS_VL_T_Sum_Std',                            'SS_VL_T_Mean_Sum', 'SS_VL_T_Mean_Mean', 'SS_VL_T_Mean_Std',                            'SS_VL_T_Std_Sum', 'SS_VL_T_Std_Mean', 'SS_VL_T_Std_Std',                            'VL_time_diff', 'browse_diff', 'first_last_diff',]]
    
    y = class_data_1.loc[:,'transaction'].copy()
    
    return(X,y)
    


# In[4]:


def classifier_training(clf, X_train, y_train):
    """
    This function take the classifier model, X features and y target for training the machine learning model.
    This function performs a single .fit action and returns the fitted model.
    
    Parameters
    ----------
    clf : classifier
        The machine learning model selected for training.
    
    X_train : dataframe object (pandas) or array values
        The X from the load_data().
    
    y_train : dataframe object (pandas) or array values
        The y from the load_data().    
    
    Return
    ------    
    clf : classifier model
        The trained/fitted machine learning model.
    
    """
    
    seed = 18
    clf.fit(X_train, y_train)
    return(clf)


# In[5]:


def predict_results(clf, X_test):
    """
    This function take the trained classifier model and the X features to predict the possible outcome for
    the features (generate the y).
    
    
    Parameters
    ----------
    clf : classifier
        The trained machine learning model from classifier_training() function.
    
    X_test : dataframe object (pandas) or array values
        The input features.
    
    y_train : dataframe object (pandas) or array values
        The y from the load_data().    
    
    
    Return
    ------    
    y_pred_class : predicted classes
        The predicted classes by the machine learning model. Usually 1 or 0.
        
    y_pred_proba : predicted probablities values
        The predicted probability of being class 1.
    
    """
    # class predictions and predicted probabilities
    seed = 18
    y_pred_class = clf.predict(X_test)
    y_pred_proba = clf.predict_proba(X_test)
    return(y_pred_class, y_pred_proba)


# In[6]:


def random_undersampler(X, y):
    """
    The function accepts the X and y to be undersamples based on the least available target
    numbers. For example, if there are 12,000 of target value of 1 and 234,000 of target value
    of 0, the RandomUnderSampler() selected only 12,000 of target value of 0. Make sure the 
    following library is loaded first:
        
        from imblearn.under_sampling import RandomUnderSampler

    
    Parameters
    ----------
    X : dataframe object (pandas) or array values
        X (feature parameters) which are the training/test features.
        
    y : dataframe object (pandas) or array values
        y (target parameter) which are the target output.
    
    
    Return
    ------    
    X_res : array values
        X (feature parameters) training/test features which have been sampled.
        
    y_res : dataframe object (pandas) or array values
        y (target parameter) which have been sampled.
        
        
    """


    seed = 18
    rus = RandomUnderSampler(random_state=seed)
    X_res, y_res = rus.fit_resample(X, y)
    print('Original dataset shape %s' % Counter(y))
    print('Resampled dataset shape %s' % Counter(y_res))
    
    return(X_res, y_res)


# In[7]:


def classifier_initialization():
    """
    This function initializes the classifier model. Make sure the following the libraries are
    loaded first:

        from sklearn.tree import DecisionTreeClassifier
        from mlxtend.classifier import EnsembleVoteClassifier
        from sklearn.ensemble import RandomForestClassifier
        from sklearn.ensemble import ExtraTreesClassifier
        from sklearn.ensemble import AdaBoostClassifier
        from sklearn.ensemble import GradientBoostingClassifier
        import xgboost as xgb    
    
    
    Parameters
    ----------
    None.
    
    
    Return
    ------    
    clf : classifier model
        The ensemble model using boosting method by combining several other classifier method.
        
        
    """
    
    seed = 18

    xgb_boost = xgb.XGBClassifier(learning_rate=0.2, min_samples_leaf=1, max_depth=3,                                   n_estimators=50, random_state=seed, n_jobs=-1)
    
    return(xgb_boost)


# In[8]:


def test_accuracy(clf,y_true,X):
    """
    This function test the accuracy and scores the performance of the ensemble machine learning
    model. Before loading the function, make sure the following the libraries are loaded first:
    
        from sklearn.metrics import precision_score, recall_score
        from sklearn.metrics import accuracy_score
        from sklearn.metrics import f1_score
        from sklearn.metrics import precision_recall_fscore_support


    
    Parameters
    ----------
    clf : classifier model
        The trained ensemble model using boosting method by combining several other classifier 
        models.
        
    y_true : dataframe/series object (pandas) or array values
        The actual output or target values.
    
    X : dataframe object (pandas) or array values
        The X feature for the inputs.
    
    
    Return
    ------  
    None. Outputs are just printed.
    
        
        
    """
    y_predict = clf.predict(X)
    print('Accuracy Score = ', accuracy_score(y_true, y_predict))
    print('F1 Score = ',f1_score(y_true,y_predict,labels='binary'))
    print('Precision Score = ',precision_score(y_true,y_predict,labels='binary'))
    print('Recall Score = ',recall_score(y_true,y_predict,labels='binary'))
    print('precision_recall_fscore_support',           precision_recall_fscore_support(y_true, y_predict, average='macro'))
    
    return()


# #### What is happening?
# 
# The `xxx_notrans` dataframe is the actual search+download+transaction dataset for that month. The model created was suppose to predict which of the users in `xxx_notrans` are most likely to convert or to perform the transaction. To do this, just get the probability of `xxx_notrans` from the `test_results()` and compare against the actual in `xxx_notrans`.

# Boosting
# ==

# In[9]:


# (trans_data_page, notrans_final_page) = load_datafile("trans_2019data_auto.csv","no_trans_2019data_auto.csv")

(trans_data_page, notrans_final_page) = load_datafile("s3a://logo-generator-kj/rish/SIPtrans_data.csv","s3a://logo-generator-kj/rish/no_SIPtrans_data.csv")


# In[10]:


trans_data_page.drop(["Unnamed: 0"], axis=1, inplace=True)
notrans_final_page.drop(["Unnamed: 0"], axis=1, inplace=True)


# In[40]:


trans_data_page.head(2)


# ## Boosting with UNDERSAMPLER
# 

# In[42]:


X,y = load_training_data(trans_data_page, notrans_final_page)


# In[43]:


# Original dataset shape Counter({0.0: 2684817, 1.0: 31165})
# Resampled dataset shape Counter({0.0: 31165, 1.0: 31165})

(X_res, y_res) = random_undersampler(X.iloc[:,1::], y)


# In[44]:


X_train, X_test, y_train, y_test = train_test_split(X_res, y_res, test_size = 0.25,                                                    stratify = y_res, random_state=seed)


# In[15]:


eclf = classifier_initialization()


# In[45]:


eclf = classifier_training(eclf,X_train,y_train)


# In[46]:


test_accuracy(eclf,y_test,X_test)


# ## Boosting with SMOTE
# 

# In[17]:


def random_smote(X, y):
    """
    The function accepts the X and y to be OVER sampled based on the least available target
    numbers. For example, if there are 12,000 of target value of 1 and 234,000 of target value
    of 0, the RandomUnderSampler() selected multiple times the target value of 1. Make sure the 
    following library is loaded first:
        
        from imblearn.under_sampling import RandomOverSampler

    
    Parameters
    ----------
    X : dataframe object (pandas) or array values
        X (feature parameters) which are the training/test features.
        
    y : dataframe object (pandas) or array values
        y (target parameter) which are the target output.
    
    
    Return
    ------    
    X_res : array values
        X (feature parameters) training/test features which have been sampled.
        
    y_res : dataframe object (pandas) or array values
        y (target parameter) which have been sampled.
        
        
    """


    seed = 18
    
    sm = SMOTE(sampling_strategy= 'auto' ,k_neighbors = 10, random_state=seed, n_jobs=-1)
    X_res, y_res = sm.fit_resample(X, y)
    print('Original dataset shape %s' % Counter(y))
    print('Resampled dataset shape %s' % Counter(y_res))
    
    return(X_res, y_res)


# In[18]:


X_smt,y_smt = load_training_data(trans_data_page, notrans_final_page)


# In[19]:


# Original dataset shape Counter({0.0: 2684817, 1.0: 31165})
# Resampled dataset shape Counter({0.0: 31165, 1.0: 31165})

(X_res_smt, y_res_smt) = random_smote(X_smt, y_smt)


# In[20]:


X_train_smt, X_test_smt, y_train_smt, y_test_smt = train_test_split(X_res_smt, y_res_smt, test_size = 0.25,                                                                    stratify = y_res_smt, random_state=seed)


# In[21]:


eclf_smt = classifier_initialization()


# In[22]:


eclf_smt = classifier_training(eclf_smt,X_train_smt,y_train_smt)


# In[23]:


test_accuracy(eclf_smt,y_test_smt,X_test_smt)


# In[ ]:





# Testing the model
# ==

# In[47]:


def load_testing_data(file_1):
    """

    
    """
    
    seed = 18
    class_data_1 = file_1[file_1.SS_T_Sum_Sum < 10900].copy()
    class_data_1.reset_index(inplace=True)
    class_data_1.drop(['index'], axis=1, inplace=True)
    class_data_1 = class_data_1[~(class_data_1.SS_T_Sum_Sum < 0)].copy()
    class_data_1.reset_index(inplace=True)
    class_data_1.drop(['index'], axis=1, inplace=True)
    X = class_data_1.loc[:,['user_id',                            'SS_Page_Count_Sum', 'SS_Page_Count_Mean','SS_Page_Count_Std',                            'SS_T_Sum_Sum', 'SS_T_Sum_Mean', 'SS_T_Sum_Std',                             'SS_T_Mean_Sum', 'SS_T_Mean_Mean', 'SS_T_Mean_Std',                             'SS_T_Std_Sum', 'SS_T_Std_Mean', 'SS_T_Std_Std',                             'SS_DL_Count_Sum',                            'SS_VL_Count_Sum', 'SS_VL_Count_Mean', 'SS_VL_Count_Std',                            'SS_VL_T_Sum_Sum', 'SS_VL_T_Sum_Mean', 'SS_VL_T_Sum_Std',                            'SS_VL_T_Mean_Sum', 'SS_VL_T_Mean_Mean', 'SS_VL_T_Mean_Std',                            'SS_VL_T_Std_Sum', 'SS_VL_T_Std_Mean', 'SS_VL_T_Std_Std',                            'VL_time_diff', 'browse_diff', 'first_last_diff',]]
    
    y = class_data_1.loc[:,'transaction'].copy()
    
    return(X,y)
    


# In[63]:


def output_file(user_data, y_class, y_proba, real_y):
    """
    
    """

    user_data = pd.concat([user_data,pd.Series(y_class)], axis=1, ignore_index=True)
    user_data = pd.concat([user_data,pd.DataFrame(y_proba)], axis=1, ignore_index=True)
    user_data = pd.concat([user_data,pd.Series(real_y)], axis=1, ignore_index=True)

    user_data.rename(columns={0 : 'user_id', 1 : 'class', 2 : 'proba_0', 3 : 'proba_1', 4: 'actual'}, inplace=True)
    user_data_group = user_data.groupby(['user_id']).mean().reset_index()
#    user_data_group.to_csv("s3a://logo-generator-kj/rish/churn_hotlist.csv", index=False)
    return(user_data_group)


# In[25]:


test_trans = pd.read_csv("s3a://logo-generator-kj/rish/nov_trans_data.csv", index_col = 0)


# In[27]:


test_notrans = pd.read_csv("s3a://logo-generator-kj/rish/nov_notrans_data.csv", index_col = 0)


# In[48]:


X_nov, y_nov = load_testing_data(test_trans)


# In[50]:


test_accuracy(eclf,y_nov,X_nov.iloc[:,1::].values)


# In[73]:


test_output = output_file(X_nov.iloc[:,0],                          eclf.predict(X_nov.iloc[:,1::].values),                          eclf.predict_proba(X_nov.iloc[:,1::].values),                          y_nov)


# In[75]:


test_output


# In[79]:


test_output.describe()


# In[ ]:





# In[34]:


test_accuracy(eclf_smt,y_nov,X_nov.values)


# In[80]:


test_output = output_file(X_nov.iloc[:,0],                          eclf_smt.predict(X_nov.iloc[:,1::].values),                          eclf_smt.predict_proba(X_nov.iloc[:,1::].values),                          y_nov)


# In[81]:


test_output.describe()


# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[83]:


X_nov_no, y_nov_no = load_testing_data(test_notrans)


# In[35]:


test_accuracy(eclf,y_nov_no,X_nov_no.values)


# In[84]:


test_output = output_file(X_nov_no.iloc[:,0],                          eclf_smt.predict(X_nov_no.iloc[:,1::].values),                          eclf_smt.predict_proba(X_nov_no.iloc[:,1::].values),                          y_nov_no)


# In[85]:


test_output.describe()


# In[36]:


test_accuracy(eclf_smt,y_nov_no,X_nov_no.values)


# In[80]:


test_output = output_file(X_nov.iloc[:,0],                          eclf_smt.predict(X_nov.iloc[:,1::].values),                          eclf_smt.predict_proba(X_nov.iloc[:,1::].values),                          y_nov)


# In[81]:


test_output.describe()


# In[ ]:





# In[ ]:





# In[ ]:





# In[86]:


X_nov_all, y_nov_all = load_training_data(test_trans, test_notrans)


# In[87]:


test_accuracy(eclf,y_nov_all,X_nov_all.iloc[:,1::].values)


# In[88]:


test_accuracy(eclf_smt,y_nov_all,X_nov_all.iloc[:,1::].values)


# In[89]:


test_output = output_file(X_nov_all.iloc[:,0],                          eclf_smt.predict(X_nov_all.iloc[:,1::].values),                          eclf_smt.predict_proba(X_nov_all.iloc[:,1::].values),                          y_nov_all)


# In[90]:


test_output.describe()


# In[91]:


test_output = output_file(X_nov_all.iloc[:,0],                          eclf.predict(X_nov_all.iloc[:,1::].values),                          eclf.predict_proba(X_nov_all.iloc[:,1::].values),                          y_nov_all)


# In[92]:


test_output.describe()


# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# Export the model out
# ==

# In[93]:


import joblib


# In[94]:


joblib.dump(eclf, 'search_model_2019.joblib')


# In[ ]:


import sklearn
print(sklearn.__version__)


# In[ ]:





# Miscellaneous
# ==

# ### Test with 2019 version 2.0

# In[ ]:


(sep_trans, sep_notrans) = load_datafile("sep_trans_2019data_auto.csv","sep_no_trans_2019data_auto.csv")


# In[ ]:


X,y = load_training_data(sep_trans, sep_notrans)


# In[ ]:


(X_res, y_res) = random_undersampler(X, y)


# In[ ]:


test_accuracy(eclf,y_res,X_res)


# In[ ]:





# In[ ]:





# In[ ]:





# ### Test with 2019 version 1.0
# 

# This is to test the machine learning model using the `test_accuracy(clf,y_true,X)` function. In the case below, we are using the `trans_data_2019_auto.csv` and `no_trans_data_2019_auto.csv` data file.

# In[ ]:


#trained used X_res
test_accuracy(eclf,y_test,X_test)


# In[ ]:


# trans_data_page = pd.read_csv("trans_data_all.csv")
# notrans_final_page = pd.read_csv("notrans_data_all.csv")

trans_data_2019 = pd.read_csv("trans_data_auto.csv")
notrans_data_2019 = pd.read_csv("no_trans_data_auto.csv")


# In[ ]:


trans_data_2019.SS_T_First = pd.to_datetime(trans_data_2019.SS_T_First)
notrans_data_2019.SS_T_First = pd.to_datetime(notrans_data_2019.SS_T_First)


# In[ ]:


#trans_data_2019.info()


# In[ ]:


X_2019,y_2019 = load_training_data(trans_data_2019, notrans_data_2019)


# In[ ]:


eval_trans = trans_data_2019[trans_data_2019.SS_T_First.dt.month == 6]


# In[ ]:


eval_notrans = notrans_data_2019[notrans_data_2019.SS_T_First.dt.month == 6]


# In[ ]:


ttx = eval_trans.loc[:,['SS_Page_Count_Sum', 'SS_Page_Count_Mean','SS_Page_Count_Std',                        'SS_T_Sum_Sum', 'SS_T_Sum_Mean', 'SS_T_Sum_Std',                         'SS_T_Mean_Sum', 'SS_T_Mean_Mean', 'SS_T_Mean_Std',                         'SS_T_Std_Sum', 'SS_T_Std_Mean', 'SS_T_Std_Std', 'browse_diff', 'first_last_diff',]]


# In[ ]:


tty =  eval_trans.loc[:, 'transaction']


# In[ ]:


test_accuracy(eclf,tty,ttx.values)


# In[ ]:





# In[ ]:


ttx = eval_notrans.loc[:,['SS_Page_Count_Sum', 'SS_Page_Count_Mean','SS_Page_Count_Std',                          'SS_T_Sum_Sum', 'SS_T_Sum_Mean', 'SS_T_Sum_Std',                           'SS_T_Mean_Sum', 'SS_T_Mean_Mean', 'SS_T_Mean_Std',                           'SS_T_Std_Sum', 'SS_T_Std_Mean', 'SS_T_Std_Std', 'browse_diff', 'first_last_diff',]]


# In[ ]:


tty =  eval_notrans.loc[:, 'transaction']


# In[ ]:


test_accuracy(eclf,tty,ttx.values)


# In[ ]:


output = eclf.predict(ttx.values)


# In[ ]:


print(output[output == 1].shape)
print(output.shape)
print(output[output == 1].shape[0]/output.shape[0])


# In[ ]:


f1_score(tty,output,labels='binary')


# In[ ]:


precision_recall_fscore_support(tty,output)


# In[ ]:





# In[ ]:




