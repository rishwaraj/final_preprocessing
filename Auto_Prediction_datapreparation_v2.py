#!/usr/bin/env python
# coding: utf-8

# Preparing data to predict transactions
# ==
# 
# An investigation to see the tends and behaviour that lead to first transaction.
# 
# ### Objectives
# Predict who is the most likely to make a transaction, specifically targeted towards the new
# users.
# 
# ### Requirement
# The dataset/database that will be considered and applied in the prediction is search-log data:
# 
# 1. search log
# - view log
# - download log
# - user log (a list of user_id and corresponding uid)
# - sales log
# 
# 
# ### Features
# The search log data is passed to KJ's algorithm for preprocessing after Jazz's team's preprocessing sequence. This generates the fasttext dataframes level 2 dataset/data which are used in this analysis. The analysis is particularly focused on few important features, namely:
# 
# 1. `Page_count` in both level 1 and level 2 fasttext data.
# 2. `T_Sum` in the level 1 and `SS_T_Sum` in level 2 fasttext data.
# 3. `T_Mean` in the level 1 and `SS_T_Mean` in level 2 fasttext data.
# 4. `T_Std` in in the level 1 and `SS_T_Std` in level 2 fasttext data.
# 
# The #2 to #4 above is repeated for `view_log` as well. 
# 
# 5. The time difference between each `browsing_id` session, labelled as `browse_diff` and time difference between `T_First` and `T_Last` in both level 1 and the corresponding `SS_T_First` and `SS_T_Last` in level 2 fasttext. 
# 6. Also looks into the VL series which is the data derived from the `view_log`. The `view_diff` is the time different between first and last time images viewed i.e.`SS_VL_T_First` and `SS_VL_T_Last` in level 2 fasttext 
# 
# The features above acts as the main data for the behaviour analysis. This data need to be predict the probability of NEWLY registered users to convert i.e. make their first transaction, more specifically, make a transaction within the first 14 days of registration. With that in mind, this analysis/prediction focuses on new registered user whom have not made any transaction.
# 
# The above five features are not the only features used in the study. 
# 
# All the features from the level 2 data file are as follow :
# 
# `['user_id', 'uid_y', 'browsing_id','SS_T_First', 'SS_T_Last','SS_DL_T_First','SS_DL_T_Last', 'SS_Page_Count_Sum','SS_Page_Count_Mean', 'SS_Page_Count_Std','SS_T_Sum_Sum', 'SS_T_Sum_Std', 'SS_T_Mean_Sum', 'SS_T_Mean_Std', 'SS_T_Std_Sum', 'SS_T_Std_Std', 'SS_DL_Count_Sum', 'SS_T_Sum_Mean', 'SS_T_Mean_Mean', 'SS_T_Std_Mean', 'SS_VL_Count_Sum', 'SS_VL_Count_Mean', 'SS_VL_Count_Std', 'SS_VL_T_Sum_Sum', 'SS_VL_T_Sum_Mean', 'SS_VL_T_Sum_Std', 'SS_VL_T_Mean_Sum', 'SS_VL_T_Mean_Mean', 'SS_VL_T_Mean_Std', 'SS_VL_T_Std_Sum', 'SS_VL_T_Std_Mean', 'SS_VL_T_Std_Std', 'SS_VL_T_First', 'SS_VL_T_Last', 'view_diff','browse_diff', 'first_last_diff']`
# 
# Most of the features related to sales such as `['packagetype','package','fusd_net','transtype','country', 'purchasetype','user_name','pdate','reg_date','reg_pdate_diff','TR_T_First','TR_T_Last']` can be ignored. These data is only required for training the prediction model through labeling.

# In[1]:


import pandas as pd
import numpy as np
import zipfile
import s3fs


# Transaction Analysis
# ==
# This is to analysis and predict which users are going to convert based on the search log data.

# In[2]:


def load_search(file_path):
    """
    This function reads the csv file from the s3 bucket and converts the dates columns
    to datetime type  values. Returns a dataframe with datetime columns converted.
    
    Parameters
    ----------
    month : str
        The month in which the data is to be loaded. Just enter the first three alphabets of the
        month, without any caplocks: eg. January = 'jan', September = 'sep'


    Returns
    -------
    Dataframe object
        The dataframe of the requested month.
        
    """
    month_2018 = pd.read_csv(file_path)
    month_2018.SS_T_First = pd.to_datetime(month_2018.SS_T_First)
    month_2018.SS_T_Last = pd.to_datetime(month_2018.SS_T_Last)
    month_2018.SS_DL_T_First = pd.to_datetime(month_2018.SS_DL_T_First)
    month_2018.SS_DL_T_Last = pd.to_datetime(month_2018.SS_DL_T_Last)
#    may_sale1.SS_VL_T_First.fillna(0.0)
    month_2018.SS_VL_T_First = pd.to_datetime(month_2018.SS_VL_T_First)
#    may_sale1.SS_VL_T_Last.fillna(0.0)
    month_2018.SS_VL_T_Last = pd.to_datetime(month_2018.SS_VL_T_Last)

    return(month_2018)


# In[3]:


def load_sales(file_path):
    """
    This function retrieves the sales data from the s3 bucket and saves it as a pandas dataframe.
    The input given is the location of the data file in the s3 bucket. 
    
    *Important note: this function allocates specific column names based on the data
     file given at that time. Look at the sale data file before running this file. If colnames
     are required, make changes to the read_csv command.* 
    
    For example, the object url of the 2018 sales file is in the following:   
        https://123rf-search-log-dumps.s3.amazonaws.com/output/sales.csv
    
    Convert this into pandas readable format (make sure to use 's3a', not 's3'):
        s3a://123rf-search-log-dumps/output/sales.csv
        
    
    Alternatively, just open the file in bucket from browser and look for the 'copy path' option 
    from the file 'Overview' tab.
    
    The difference between this function sales_data_2018() is that this file specifically
    defines the column names.
    
    The relevant feature is also converted to datetime format.
    
    
    Parameters
    ----------
    file_path : str
        The file path for the data file. Data file must be a .csv file in s3 bucket.
        
    Return
    ------
    sales_2019 : dataframe object (pandas)
        Returns pandas dataframe.
    
    
    """
    
#     colname = ['sales_id', 'uid', 'pdate', 'country', 'packagetype', 'package',\
#                'currency', 'fusd_net', 'purchasetype', 'transtype']

    sales_2019 = pd.read_csv(file_path)
    sales_2019.pdate = pd.to_datetime(sales_2019.pdate)
    return(sales_2019)


# In[4]:


def load_user(file_path):
    """
    This function retrieves the user data file from the s3 bucket and saves it as a pandas 
    dataframe. The user data file basically contains the user_name and the corresponding
    user_id (number id) for 123RF registered users.
    
    Make sure the data file check the header. If the data doesn't have any header, you need
    to include the header.
    
    The data file (csv file) should be in the following manner:
    ________________________
    |90833 |ig0rzh_        | 
    |91852 |marta_         |
    |624333|_maximp_       |
    |533325|+380732553995  |
    |454782|+77051110800   |
    
    The input given is the location of the data file in the s3 bucket. 
    
    For example, the object url for the user file is in the following:   
        https://123rf-search-log-dumps.s3.amazonaws.com/output/misc_201907/user.csv
    
    Convert this into pandas readable format (make sure to use 's3a', not 's3'):
        s3a://123rf-search-log-dumps/output/misc_201907/user.csv
    
    Alternatively, just open the file in bucket from browser and look for the 'copy path' option 
    from the file 'Overview' tab.
    
    
    Parameters
    ----------
    file_path : str
        The file path for the data file. Data file must be a .csv file in s3 bucket.
        
    Return
    ------
    user : dataframe object (pandas)
        Returns pandas dataframe.
    
    
    """
    
    user = pd.read_csv(file_path)
    return(user)


# In[5]:


def search_date(search_data, year, month):
    """
    This function merges the process search_data data(frame) from the sales_user() function and
    merges it with the sales data for the selected year and month.
    
    Parameters
    ----------
    search_data : dataframe object (pandas)
        Pandas dataframe object generated by the sales_user() function.
    
    sales : dataframe object (pandas)
        Pandas dataframe object from the sales data file.
        
    year : int
        The selected year.
        
    month: int
        The selected month.
        
    1
    Returns
    -------
    test_1 : dataframe object (pandas)
        The merged product.
    
    
    """
    
    
    temp = search_data[((search_data.SS_T_First.dt.year == year) &                        (search_data.SS_T_First.dt.month == month))].copy()
    
    return(temp)


# In[6]:


def get_features(ss_may_sale2):
    """
    This function is mainly used for feature engineering preprocessing step, namely to get the
    'browse_diff' (difference between browsing time) and 'first_last_diff' (different between
    first and last browsing session). But before getting these two features, the data has to be
    sorted using .sort_values(['user_id','SS_T_First','browsing_id']) method, according to the
    selected features.
    
    This function also selects only the following features for the machine learning prediction
    (and modeling):
    ['user_id',\
    'SS_Page_Count_Sum', 'SS_Page_Count_Mean','SS_Page_Count_Std',\
    'SS_T_Sum_Sum', 'SS_T_Sum_Mean', 'SS_T_Sum_Std', \
    'SS_T_Mean_Sum', 'SS_T_Mean_Mean', 'SS_T_Mean_Std', \
    'SS_T_Std_Sum', 'SS_T_Std_Mean', 'SS_T_Std_Std', \
    'SS_DL_Count_Sum',\
    'SS_VL_Count_Sum', 'SS_VL_Count_Mean', 'SS_VL_Count_Std',\
    'SS_VL_T_Sum_Sum', 'SS_VL_T_Sum_Mean', 'SS_VL_T_Sum_Std',\
    'SS_VL_T_Mean_Sum', 'SS_VL_T_Mean_Mean', 'SS_VL_T_Mean_Std',\
    'SS_VL_T_Std_Sum', 'SS_VL_T_Std_Mean', 'SS_VL_T_Std_Std',\
    'VL_time_diff', 'browse_diff', 'first_last_diff']
    
    
    Parameters
    ----------
    ss_may_sale2 : dataframe object (pandas)
        Pandas dataframe object generated by the search_sale() function.

    
    Returns
    -------
    ss_may_sale1_3 : dataframe object (pandas)
    
    
    """
        
    ss_may_sale1_3 = ss_may_sale2.sort_values(['user_id','SS_T_First','browsing_id'])
    
    ss_may_sale1_3['browse_diff'] = ss_may_sale1_3.SS_T_First.diff()
    ss_may_sale1_3.browse_diff = ss_may_sale1_3.browse_diff.dt.total_seconds()
    
    ss_may_sale1_3['first_last_diff'] = ss_may_sale1_3.SS_T_Last - ss_may_sale1_3.SS_T_First
    ss_may_sale1_3.first_last_diff = ss_may_sale1_3.first_last_diff.dt.total_seconds()
    
    ss_may_sale1_3['VL_time_diff'] = ss_may_sale1_3.SS_VL_T_Last - ss_may_sale1_3.SS_VL_T_First    
    ss_may_sale1_3.VL_time_diff = ss_may_sale1_3.VL_time_diff.dt.total_seconds()
    
    t_mean = ss_may_sale1_3.VL_time_diff.mean()
    ss_may_sale1_3.VL_time_diff.fillna(t_mean, inplace=True)
    
    ss_may_sale1_3 = ss_may_sale1_3.loc[:,['user_id','uid',                                           'SS_Page_Count_Sum', 'SS_Page_Count_Mean','SS_Page_Count_Std',                                           'SS_T_Sum_Sum', 'SS_T_Sum_Mean', 'SS_T_Sum_Std',                                            'SS_T_Mean_Sum', 'SS_T_Mean_Mean', 'SS_T_Mean_Std',                                            'SS_T_Std_Sum', 'SS_T_Std_Mean', 'SS_T_Std_Std',                                            'SS_DL_Count_Sum',                                           'SS_VL_Count_Sum', 'SS_VL_Count_Mean', 'SS_VL_Count_Std',                                           'SS_VL_T_Sum_Sum', 'SS_VL_T_Sum_Mean', 'SS_VL_T_Sum_Std',                                           'SS_VL_T_Mean_Sum', 'SS_VL_T_Mean_Mean', 'SS_VL_T_Mean_Std',                                           'SS_VL_T_Std_Sum', 'SS_VL_T_Std_Mean', 'SS_VL_T_Std_Std',                                           'VL_time_diff', 'browse_diff', 'first_last_diff']].copy()
    
    ss_may_sale1_3.reset_index(inplace=True)
    ss_may_sale1_3.drop(['index'], axis=1, inplace=True)
    
    first_repeat = ss_may_sale1_3[~(ss_may_sale1_3.user_id.duplicated(keep='first'))].index.to_list()
    # print(ss_may_sale1_3.iloc[1,-2] - ss_may_sale1_3.iloc[1,-2])
    ss_may_sale1_3.iloc[0,-2] = ss_may_sale1_3.iloc[1,-2] - ss_may_sale1_3.iloc[1,-2]
    ss_may_sale1_3.iloc[first_repeat,-2] =  ss_may_sale1_3.iloc[0,-2]

    return(ss_may_sale1_3)


# In[7]:


def search_Nosale(search, user, sales, year, month):
    """
    This function merges the process searc log data  i.e. month_data(frame) and merge it with the
    sales and user data. This is similar to the search_sale().
    
    Include one column named 'transaction' which carries value 0 to denote no transaction occured.
    
    Parameters
    ----------
    search : dataframe object (pandas)
        Pandas dataframe object generated by the sales_user() function.
    
    sales : dataframe object (pandas)
        Pandas dataframe object from the sales data file.
    
    user : dataframe object (pandas)
        The list of registered users and their corresponding user_id and user_name. It is loaded 
        first as pandas dataframe and then sent to this function.  
        
    year : int
        The selected year.
        
    month: int
        The selected month.
        
    
    Returns
    -------
    temp : dataframe object (pandas)
        The search log data of all the users that are not found in the sales data(frame).
    
    
    """
    
    
    # merging sales with users to get the user_id
    sales1_1 = sales.merge(df_user, how='left', left_on='uid', right_on='uid')
    
    # remove where the user_id are not available
    sales1_1.dropna(subset=['user_id'], axis=0, inplace=True) 
    sales1_1.rename(columns={'user_id': 'user_id_sales'}, inplace=True)   
    sales1_2 = sales1_1.dropna(subset = ['user_id_sales'])  
    
    # getting list/data of the users that did not make a transaction yet
    temp =  search[~(search.user_id.isin(sales1_2.user_id_sales))].copy()

    return(temp)


# In[8]:


def NO_transaction_data(name,sales_2018,user,year,month):
    """
    This function combines the search log data, sales data, user id data, registration data
    for the given month and year to create a final process dataframe for all the search log 
    data for users that did not perform any transaction. The function also calls for the 
    following supplimentary functions:
    
        1. load_data()
        2. sales_user()
        3. search_sale()
        4. get_features() 
        
    This function also removes all search log data where SS_Page_count_sum == 1. 
    
    Parameters
    ----------
    name : str
        The month in which the data is to be loaded. Just enter the first three alphabets of the
        month, without any caplocks: eg. January = 'jan', September = 'sep'
    
    sales_2018 : dataframe object (pandas)
    
    user : dataframe object (pandas)
        The list of registered users and their corresponding user_id and user_name. It is loaded as
        pandas dataframe.    
    
    register : dataframe object (pandas)
        The registration data of the users. Must include the date of registration. It is loaded as
        pandas dataframe.    
        
    year : int
        The target year for analysis.
        
        
    month : int
        The target month for analysis.
    
    
    Return
    ------
    may : dataframe object (pandas)
    
    
    """
    
    may_2018 = load_search(name)    
    may_NOsale = search_Nosale(may_2018,user,sales_2018,year,month)
    may_NOsale = get_features(may_NOsale)
    may_NOsale = may_NOsale[~(may_NOsale.SS_Page_Count_Sum == 1)].copy()
    return(may_NOsale)


# Testing the functions using Search Log data
# ===
# 
# Save the following:
# 1. `xxx_sales_1` as .csv file for first ever transactions done (looks at `purchasetype`==1).
# 2. `xxx_sales_0` as .csv file for repeated transactions done (looks at `purchasetype`==0).
# 3. `xxx_NoSale` as .csv file for non-transaction (basically, all the `user_id` not found in `sales.csv` data).
# 
# where `xxx` in the above refers to the months from May 2018 to Jun 2019 (so for May of 2018, `xxx_sales_1` is `may2018_sales_1`.
# 
# At the end of the dataframe, just before saving to csv file, make sure to add a final feature coulunn `transaction` to the dataframe with the following values:
# 1. `transaction = 1` for first transaction.
# 2. `transaction = 2` for repeated transaction.
# 3.  `transaction = 0` for no transactions.

# In[9]:


# s3a://logo-generator-kj/rish/jun2019_w1.csv
file_path = str(input("Enter the file path to the s3 bucket for the search data file: "))


# In[10]:


df_sales_2019 = load_sales("s3://logo-generator-kj/rish/nov_sales.csv")


# In[ ]:


#df_sales_2019.head(1)


# In[11]:


df_user = load_user("s3a://123rf-search-log-dumps/output/misc_201911/user.csv")


# In[ ]:


#df_user.head(1)


# In[ ]:


# df_register = register_data("s3://123rf-search-log-dumps/output/misc_201911/member.csv")


# In[12]:


nov = NO_transaction_data(file_path,df_sales_2019,df_user,2019,11)


# In[14]:


nov.head(1)


# In[13]:


nov.to_csv("s3a://logo-generator-kj/rish/week48_search.csv", index=False)


# Some test
# ==

# In[ ]:


may_2018 = load_search(file_path)    
may_NOsale = search_Nosale(may_2018,df_user,df_sales_2019,2019,6)
may_NOsale = get_features(may_NOsale)


# In[ ]:


may_NOsale


# In[ ]:


df_user[df_user.user_name == '020estudio']


# In[ ]:


may_NOsale[may_NOsale.user_id == 626835]


# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:




