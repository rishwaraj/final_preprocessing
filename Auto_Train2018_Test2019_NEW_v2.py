#!/usr/bin/env python
# coding: utf-8

# Looking at what leads to transactions
# ==
# 
# An investigation to see the tends and behaviour that lead to transactions where first time transaction is labelled as `purchasetype == 1` and repeated transactions (2nd and following) is labelled as `purchasetype == 0` in the `sales.csv` data.
# 
# There are several dataset/database that will be considered and applied in building the dataframe for the analysis. The dataset/database are namely:
# 1. sales - all the sales data here, including the purchasetype that indicates first purchase/transaction or repeated transaction.
# 2. user - to get the username to the corresponding user_id. 
# 3. registration - to get the registration data of the user. Required to compare when the first transaction occured since registration.
# 4. search log - the fasttext dataframes that KJ designed. Both level 1 and level 2 dataset/data are used in this analysis i.e. two separate analysis are done.
# 
# The analysis is particularly focused on few important features, namely:
# 1. `Page_count` in both level 1 and level 2 fasttext data.
# 2. `T_Sum` in the level 1 and `SS_T_Sum` in level 2 fasttext data.
# 3. `T_Mean` in the level 1 and `SS_T_Mean` in level 2 fasttext data.
# 4. `T_Std` in in the level 1 and `SS_T_Std` in level 2 fasttext data.
# 5. `'packagetype','package','fusd_net','transtype','country','purchasetype','user_name','pdate'` from the sales data.
# 5. The time difference between each `browsing_id` session, labelled as `browse_diff` and time difference between `T_First` and `T_Last` in both level 1 and the corresponding `SS_T_First` and `SS_T_Last` in level 2 fasttext. 
# 
# The features above acts as the main data for the behaviour analysis. This data need to be build for both first time purchase or `purchasetype == 1`, and repeated transactions (2nd and following) or `purchasetype == 0`. The remaining data (of the users that did not perform any transactions) are saved separately as the NoTransaction data. 
# 
# The five features are not the only features used in the study. 
# 
# For fasttext level one, all the features are: `['user_id','country_id','uuid','puuid','browsing_id','Seq_page','packagetype','package','fusd_net',
# 'transtype','country', 'purchasetype','user_name','pdate','reg_date','reg_pdate_diff', 'T_First','T_Last','Page_count','T_Sum','T_Mean','T_Std','DL_count']`.
# 
# 
# For fasttext level two, all the features are:`['user_id','browsing_id','packagetype','package','fusd_net','transtype','country', 'purchasetype','user_name','pdate','reg_date','reg_pdate_diff','TR_T_First','TR_T_Last','SS_T_First',
# 'SS_T_Last','SS_Page_Count_Sum','SS_Page_Count_Mean','SS_Page_Count_Std', 'SS_T_Sum_Sum','SS_T_Sum_Std', 'SS_T_Mean_Sum','SS_T_Mean_Std', 'SS_T_Std_Sum','SS_T_Std_Std', 'SS_DL_Count_Sum']`

# In[1]:


import pandas as pd
import numpy as np
import random
import matplotlib.pyplot as plt
import timeit
import zipfile
import s3fs
from zipfile import ZipFile

get_ipython().run_line_magic('matplotlib', 'inline')


# Transaction Analysis
# ==
# This is to analysis the number of users (`nunique()`) for transaction data corrected with the search log data.

# In[2]:


def load_data(month):
    """
    This function reads the csv file from the s3 bucket and converts the dates columns
    to datetime type  values. Returns a dataframe with datetime columns converted.
    
    Parameters
    ----------
    month : str
        The month in which the data is to be loaded. Just enter the first three alphabets of the
        month, without any caplocks: eg. January = 'jan', September = 'sep'


    Returns
    -------
    Dataframe object
        The dataframe of the requested month.
        
    """
    month_2018 = pd.read_csv("s3://logo-generator-kj/churn_prediction/fasttext_search_sequence/"+month+"_19_fasttext_combined_level_2.csv", low_memory=False)
    
    month_2018.SS_T_First = pd.to_datetime(month_2018.SS_T_First)
    month_2018.SS_T_Last = pd.to_datetime(month_2018.SS_T_Last)
    month_2018.SS_DL_T_First = pd.to_datetime(month_2018.SS_DL_T_First)
    month_2018.SS_DL_T_Last = pd.to_datetime(month_2018.SS_DL_T_Last)
#    may_sale1.SS_VL_T_First.fillna(0.0)
    month_2018.SS_VL_T_First = pd.to_datetime(month_2018.SS_VL_T_First)
#    may_sale1.SS_VL_T_Last.fillna(0.0)
    month_2018.SS_VL_T_Last = pd.to_datetime(month_2018.SS_VL_T_Last)
    print(month_2018.SS_VL_T_First.dtype)

    return(month_2018)


# In[3]:


def sales_user(sales_data, df_user, register, year, month):
    """
    This function takes in three different dataframes and the year, month in question and merges
    them to form one dataframe:
    a. merges sales data(frame) and user data(frame) to get the user_id for each of the users
       in the sales data to form the new sales_user data(frame).
    b. merges the sales_user data(frame) with the registration data(frame) to get the information
       on when each users have registered. Then filter out the data(frame) to get the data of new
       users whom have made a transaction within 14 days of registration.
       
    Parameters
    ----------
    sales_data : dataframe object (pandas)
        The sales data, loaded in as pandas dataframe.
        
    df_user : dataframe object (pandas)
        The list of registered users and their corresponding user_id and user_name. It is loaded as
        pandas dataframe.    
    
    register : dataframe object (pandas)
        The registration data of the users. Must include the date of registration. It is loaded as
        pandas dataframe.    
        
    year : int
        The target year for analysis.
        
        
    month : int
        The target month for analysis.
    
    Returns
    -------
    Returns two dataframe.
    
    sales1_4 : dataframe object (pandas)
        A pandas dataframe for purchasetype 1. 
    
    sales0_2 :: dataframe object (pandas)
        A pandas dataframe for purchasetype 0. 
    
    """
    
    # sales_data = search + sales data
    sales1= sales_data[((sales_data.pdate.dt.year == year) &
                        (sales_data.pdate.dt.month == month) &
                        (sales_data.purchasetype == 1))].copy()

    sales0= sales_data[((sales_data.pdate.dt.year == year) &
                        (sales_data.pdate.dt.month == month) &
                        (sales_data.purchasetype == 0))].copy()
    sales1['transaction'] = 1
    sales0['transaction'] = 1
    # merging sales with users to get the user_id
    sales1_1 = sales1.merge(df_user, how='left', left_on='uid', right_on='user_name')
    # remove where the user_id are not available
    sales1_1.dropna(subset=['user_id'], axis=0, inplace=True) 
    sales1_1.rename(columns={'user_id': 'user_id_sales'}, inplace=True)   
    sales1_2 = sales1_1.dropna(subset = ['user_id_sales'])
    
    # get the registration data into this
    sales1_3 = sales1_2.merge(register, how='left', left_on='user_name', right_on='uid')
    sales1_3['reg_pdate_diff'] = sales1_3.pdate - sales1_3.date_register
    sales1_4 = sales1_3[sales1_3.reg_pdate_diff.dt.days < 14].copy()
    
    # merging sales with users to get the user_id
    sales0_1 = sales0.merge(df_user, how='left', left_on='uid', right_on='user_name')
    # remove where the user_id are not available
    sales0_1.dropna(subset=['user_id'], axis=0, inplace=True) 
    sales0_1.rename(columns={'user_id': 'user_id_sales'}, inplace=True) 
    sales0_2 = sales0_1.dropna(subset = ['user_id_sales'])
    
#     # get the registration data into this
#     sales0_3 = sales0_2.merge(register, how='left', left_on='user_name', right_on='user')
#     sales0_3['reg_pdate_diff'] = sales0_3.pdate - sales0_3.reg_date
#     sales0_4 = sales0_3[sales0_3.reg_pdate_diff.dt.days < 10]    
        
#     sales1_4.drop(['sales_id', 'currency'],axis=1,inplace=True)
#     sales0_2.drop(['sales_id', 'currency'],axis=1,inplace=True)
    return(sales1_4, sales0_2)
     


# In[4]:


def search_sale(search_data, sales, year, month):
    """
    This function merges the process search_data data(frame) from the sales_user() function and
    merges it with the sales data for the selected year and month.
    
    Parameters
    ----------
    search_data : dataframe object (pandas)
        Pandas dataframe object generated by the sales_user() function.
    
    sales : dataframe object (pandas)
        Pandas dataframe object from the sales data file.
        
    year : int
        The selected year.
        
    month: int
        The selected month.
        
    
    Returns
    -------
    test_1 : dataframe object (pandas)
        The merged product.
    
    
    """
    
    
    temp = search_data[((search_data.SS_T_First.dt.year == year) &                        (search_data.SS_T_First.dt.month == month))].copy()
    sales.user_id_sales = sales.user_id_sales.astype('int')
    temp.user_id = temp.user_id.astype('int')
    test_0 = temp.merge(sales, how='left', left_on='user_id', right_on='user_id_sales')
    test_1 = test_0.dropna(subset=['user_id_sales'])
    
    return(test_1)


# In[5]:


def get_features(ss_may_sale2):
    """
    This function is mainly used for feature engineering preprocessing step, namely to get the
    'browse_diff' (difference between browsing time) and 'first_last_diff' (different between
    first and last browsing session). But before getting these two features, the data has to be
    sorted using .sort_values(['user_id','SS_T_First','browsing_id']) method, according to the
    selected features.
    
    This function also selects only the following features for the machine learning prediction
    (and modeling):
    ['user_id',\
    'SS_Page_Count_Sum', 'SS_Page_Count_Mean','SS_Page_Count_Std',\
    'SS_T_Sum_Sum', 'SS_T_Sum_Mean', 'SS_T_Sum_Std', \
    'SS_T_Mean_Sum', 'SS_T_Mean_Mean', 'SS_T_Mean_Std', \
    'SS_T_Std_Sum', 'SS_T_Std_Mean', 'SS_T_Std_Std', \
    'SS_DL_Count_Sum',\
    'SS_VL_Count_Sum', 'SS_VL_Count_Mean', 'SS_VL_Count_Std',\
    'SS_VL_T_Sum_Sum', 'SS_VL_T_Sum_Mean', 'SS_VL_T_Sum_Std',\
    'SS_VL_T_Mean_Sum', 'SS_VL_T_Mean_Mean', 'SS_VL_T_Mean_Std',\
    'SS_VL_T_Std_Sum', 'SS_VL_T_Std_Mean', 'SS_VL_T_Std_Std',\
    'VL_time_diff', 'browse_diff', 'first_last_diff']
    
    
    Parameters
    ----------
    ss_may_sale2 : dataframe object (pandas)
        Pandas dataframe object generated by the search_sale() function.

    
    Returns
    -------
    ss_may_sale1_3 : dataframe object (pandas)
    
    
    """
        
    ss_may_sale1_3 = ss_may_sale2.sort_values(['user_id','SS_T_First','browsing_id'])
    
    ss_may_sale1_3['browse_diff'] = ss_may_sale1_3.SS_T_First.diff()
    ss_may_sale1_3.browse_diff = ss_may_sale1_3.browse_diff.dt.total_seconds()
    
    ss_may_sale1_3['first_last_diff'] = ss_may_sale1_3.SS_T_Last - ss_may_sale1_3.SS_T_First
    ss_may_sale1_3.SS_VL_T_Last = pd.to_datetime(ss_may_sale1_3.SS_VL_T_Last)
    ss_may_sale1_3.SS_VL_T_First = pd.to_datetime(ss_may_sale1_3.SS_VL_T_First)
    ss_may_sale1_3['VL_time_diff'] = ss_may_sale1_3.SS_VL_T_Last - ss_may_sale1_3.SS_VL_T_First
    
    ss_may_sale1_3.first_last_diff = ss_may_sale1_3.first_last_diff.dt.total_seconds()
    ss_may_sale1_3.VL_time_diff = ss_may_sale1_3.VL_time_diff.dt.total_seconds()
    t_mean = ss_may_sale1_3.VL_time_diff.mean()
    ss_may_sale1_3.VL_time_diff.fillna(t_mean, inplace=True)
    
    ss_may_sale1_3 = ss_may_sale1_3.loc[:,['user_id', 'browsing_id', 'packagetype', 'package', 'fusd_net',                                           'transtype', 'country', 'purchasetype', 'user_name', 'pdate',                                           'reg_date', 'reg_pdate_diff', 'SS_T_First','SS_T_Last',                                            'SS_Page_Count_Sum', 'SS_Page_Count_Mean','SS_Page_Count_Std',                                            'SS_T_Sum_Sum', 'SS_T_Sum_Mean', 'SS_T_Sum_Std',                                            'SS_T_Mean_Sum','SS_T_Mean_Std', 'SS_T_Mean_Mean',                                            'SS_T_Std_Sum', 'SS_T_Std_Std', 'SS_T_Std_Mean',                                            'SS_VL_Count_Sum', 'SS_VL_Count_Mean','SS_VL_Count_Std',                                           'SS_VL_T_Sum_Sum', 'SS_VL_T_Sum_Mean', 'SS_VL_T_Sum_Std',                                            'SS_VL_T_Mean_Sum','SS_VL_T_Mean_Std', 'SS_VL_T_Mean_Mean',                                            'SS_VL_T_Std_Sum', 'SS_VL_T_Std_Std', 'SS_VL_T_Std_Mean',                                            'SS_DL_Count_Sum','VL_time_diff',                                           'browse_diff', 'first_last_diff', 'transaction']].copy()
    
    ss_may_sale1_3.reset_index(inplace=True)
    ss_may_sale1_3.drop(['index'], axis=1, inplace=True)
    
    first_repeat = ss_may_sale1_3[~(ss_may_sale1_3.user_id.duplicated(keep='first'))].index.to_list()
    # print(ss_may_sale1_3.iloc[1,-2] - ss_may_sale1_3.iloc[1,-2])
    ss_may_sale1_3.iloc[0,-2] = ss_may_sale1_3.iloc[1,-2] - ss_may_sale1_3.iloc[1,-2]
    ss_may_sale1_3.iloc[first_repeat,-3] =  ss_may_sale1_3.iloc[0,-2]

    return(ss_may_sale1_3)


# In[6]:


def search_Nosale(month_data, user, sales, year, month):
    """
    This function merges the process searc log data  i.e. month_data(frame) and merge it with the
    sales and user data. This is similar to the search_sale().
    
    Include one column named 'transaction' which carries value 0 to denote no transaction occured.
    
    Parameters
    ----------
    month_data : dataframe object (pandas)
        Pandas dataframe object generated by the sales_user() function.
    
    sales : dataframe object (pandas)
        Pandas dataframe object from the sales data file.
    
    user : dataframe object (pandas)
        The list of registered users and their corresponding user_id and user_name. It is loaded 
        first as pandas dataframe and then sent to this function.  
        
    year : int
        The selected year.
        
    month: int
        The selected month.
        
    
    Returns
    -------
    temp : dataframe object (pandas)
        The search log data of all the users that are not found in the sales data(frame).
    
    
    """
    sales1 = sales[((sales.pdate.dt.year == year) & (sales.pdate.dt.month == month))]
    
    # merging sales with users to get the user_id
    sales1_1 = sales1.merge(df_user, how='left', left_on='uid', right_on='user_name')
    # remove where the user_id are not available
    sales1_1.dropna(subset=['user_id'], axis=0, inplace=True) 
    sales1_1.rename(columns={'user_id': 'user_id_sales'}, inplace=True)   
    sales1_2 = sales1_1.dropna(subset = ['user_id_sales'])  
    temp =  month_data[~(month_data.user_id.isin(sales1_2.user_id_sales))].copy()
    temp['transaction'] = 0
    return(temp)


# In[7]:


def transaction_data(name,sales_2018,user,register,year,month):
    """
    This function combines the search log data, sales data, user id data, registration data
    for the given month and year to create a final process dataframe  for all the search log 
    data for users that performed any transaction. The function also calls for the following 
    supplimentary functions:
    
        1. load_data()
        2. sales_user()
        3. search_sale()
        4. get_features() 
        
    This function also removes all search log data where SS_Page_count_sum == 1. Also excluded 
    from the search log where search data starting after the sales transaction.
    
    Parameters
    ----------
    name : str
        The month in which the data is to be loaded. Just enter the first three alphabets of the
        month, without any caplocks: eg. January = 'jan', September = 'sep'
    
    sales_2018 : dataframe object (pandas)
    
    user : dataframe object (pandas)
        The list of registered users and their corresponding user_id and user_name. It is loaded as
        pandas dataframe.    
    
    register : dataframe object (pandas)
        The registration data of the users. Must include the date of registration. It is loaded as
        pandas dataframe.    
        
    year : int
        The target year for analysis.
        
        
    month : int
        The target month for analysis.
    
    
    Return
    ------
    may : dataframe object (pandas)
    
    
    """
    
    may_2018 = load_data(name)
    (may_2018_1, may_2018_0) = sales_user(sales_2018,user,register,year,month)
    may_sale1 = search_sale(may_2018,may_2018_1,year,month)
    may = get_features(may_sale1)
    may = may[~(may.SS_Page_Count_Sum == 1)].copy()
    may = may.loc[may.SS_T_First <= may.pdate].copy()
    return(may)
    


# In[8]:


def oct_nov_transaction(name,sales_2018,user,register,year,month):
    """
    This function combines the search log data, sales data, user id data, registration data
    for the given month and year to create a final process dataframe  for all the search log 
    data for users that performed any transaction. The function also calls for the following 
    supplimentary functions:
    
        1. sales_user()
        2. search_sale()
        3. get_features() 
        
    This function also removes all search log data where SS_Page_count_sum == 1. Also excluded 
    from the search log where search data starting after the sales transaction.
    
    Parameters
    ----------
    name : str
        The month in which the data is to be loaded. Just enter the first three alphabets of the
        month, without any caplocks: eg. January = 'jan', September = 'sep'
    
    sales_2018 : dataframe object (pandas)
    
    user : dataframe object (pandas)
        The list of registered users and their corresponding user_id and user_name. It is loaded as
        pandas dataframe.    
    
    register : dataframe object (pandas)
        The registration data of the users. Must include the date of registration. It is loaded as
        pandas dataframe.    
        
    year : int
        The target year for analysis.
        
        
    month : int
        The target month for analysis.
    
    
    Return
    ------
    may : dataframe object (pandas)
    
    
    """
    may_2018 = name.copy()
    (may_2018_1, may_2018_0) = sales_user(sales_2018,user,register,year,month)
    may_sale1 = search_sale(may_2018,may_2018_1,year,month)
    may = get_features(may_sale1)
    may = may[~(may.SS_Page_Count_Sum == 1)].copy()
    may = may.loc[may.SS_T_First <= may.pdate].copy()
    return(may)
    


# In[9]:


def NO_transaction_data(name,sales_2018,user,year,month):
    """
    This function combines the search log data, sales data, user id data, registration data
    for the given month and year to create a final process dataframe for all the search log 
    data for users that did not perform any transaction. The function also calls for the 
    following supplimentary functions:
    
        1. load_data()
        2. sales_user()
        3. search_sale()
        4. get_features() 
        
    This function also removes all search log data where SS_Page_count_sum == 1. 
    
    Parameters
    ----------
    name : str
        The month in which the data is to be loaded. Just enter the first three alphabets of the
        month, without any caplocks: eg. January = 'jan', September = 'sep'
    
    sales_2018 : dataframe object (pandas)
    
    user : dataframe object (pandas)
        The list of registered users and their corresponding user_id and user_name. It is loaded as
        pandas dataframe.    
    
    register : dataframe object (pandas)
        The registration data of the users. Must include the date of registration. It is loaded as
        pandas dataframe.    
        
    year : int
        The target year for analysis.
        
        
    month : int
        The target month for analysis.
    
    
    Return
    ------
    may : dataframe object (pandas)
    
    
    """
    may_2018 = load_data(name)    
    may_NOsale = search_Nosale(may_2018,user,sales_2018,year,month)
    may_NOsale = get_features(may_NOsale)
    may_NOsale = may_NOsale[~(may_NOsale.SS_Page_Count_Sum == 1)].copy()
    return(may_NOsale)


# In[10]:


def oct_nov_NO_transaction_data(name,sales_2018,user,year,month):
    """
    This function combines the search log data, sales data, user id data, registration data
    for the given month and year to create a final process dataframe for all the search log 
    data for users that did not perform any transaction. The function also calls for the 
    following supplimentary functions:
    
        1. sales_user()
        2. search_sale()
        3. get_features() 
        
    This function also removes all search log data where SS_Page_count_sum == 1. 
    
    Parameters
    ----------
    name : str
        The month in which the data is to be loaded. Just enter the first three alphabets of the
        month, without any caplocks: eg. January = 'jan', September = 'sep'
    
    sales_2018 : dataframe object (pandas)
    
    user : dataframe object (pandas)
        The list of registered users and their corresponding user_id and user_name. It is loaded as
        pandas dataframe.    
    
    register : dataframe object (pandas)
        The registration data of the users. Must include the date of registration. It is loaded as
        pandas dataframe.    
        
    year : int
        The target year for analysis.
        
        
    month : int
        The target month for analysis.
    
    
    Return
    ------
    may : dataframe object (pandas)
    
    
    """
    
    may_2018 = name.copy()
    may_NOsale = search_Nosale(may_2018,user,sales_2018,year,month)
    may_NOsale = get_features(may_NOsale)
    may_NOsale = may_NOsale[~(may_NOsale.SS_Page_Count_Sum == 1)].copy()
    return(may_NOsale)


# All Data
# ==
# 
# Getting the data in order.

# In[11]:


def sales_data_2019(file_path):
    """
    This function retrieves the sales data from the s3 bucket and saves it as a pandas dataframe.
    The input given is the location of the data file in the s3 bucket. 
    
    For example, the object url of the 2018 sales file is in the following:   
        https://123rf-search-log-dumps.s3.amazonaws.com/output/sales.csv
    
    Convert this into pandas readable format (make sure to use 's3a', not 's3'):
        s3a://123rf-search-log-dumps/output/sales.csv
        
    
    Alternatively, just open the file in bucket from browser and look for the 'copy path' option 
    from the file 'Overview' tab.
    
    The difference between this function sales_data_2018() is that this file specifically
    defines the column names.
    
    The relevant feature is also converted to datetime format.
    
    
    Parameters
    ----------
    file_path : str
        The file path for the data file. Data file must be a .csv file in s3 bucket.
        
    Return
    ------
    sales_2019 : dataframe object (pandas)
        Returns pandas dataframe.
    
    
    """

    sales_2019 = pd.read_csv(file_path, index_col=0, low_memory=False)
    sales_2019.pdate = pd.to_datetime(sales_2019.pdate)
    return(sales_2019)


# In[12]:


def user_data(file_path):
    """
    This function retrieves the user data file from the s3 bucket and saves it as a pandas 
    dataframe. The user data file basically contains the user_name and the corresponding
    user_id (number id) for 123RF registered users.
    
    Make sure the data file does not have any header. The data file (csv file) should be 
    in the following manner:
    ________________________
    |90833 |ig0rzh_        | 
    |91852 |marta_         |
    |624333|_maximp_       |
    |533325|+380732553995  |
    |454782|+77051110800   |
    
    The input given is the location of the data file in the s3 bucket. 
    
    For example, the object url for the user file is in the following:   
        https://123rf-search-log-dumps.s3.amazonaws.com/output/misc_201907/user.csv
    
    Convert this into pandas readable format (make sure to use 's3a', not 's3'):
        s3a://123rf-search-log-dumps/output/misc_201907/user.csv
    
    Alternatively, just open the file in bucket from browser and look for the 'copy path' option 
    from the file 'Overview' tab.
    
    
    Parameters
    ----------
    file_path : str
        The file path for the data file. Data file must be a .csv file in s3 bucket.
        
    Return
    ------
    user : dataframe object (pandas)
        Returns pandas dataframe.
    
    
    """
    
    user = pd.read_csv(file_path, header = None, names = ['user_id', 'user_name'], dtype={'user_id':object, 'user_name':object})
    return(user)


# In[13]:


def register_data(file_path):
    """
    This function retrieves the registration data file from the s3 bucket and saves it as a pandas 
    dataframe. The registration data file basically contains the user_name and the corresponding
    registration date for the 123RF registered users.
    
    Make sure the data file does not have any header. The data file (csv file) should be 
    in the following manner:
    ____________________________
    |123rf        |2005-06-01  | 
    |ablestock    |2005-06-01  |
    |administrator|2005-06-01  |
    |chriswaters  |2005-06-01  |
    |eskay        |2005-06-01  |
    
    
    The input given is the location of the data file in the s3 bucket. 
    
    For example, the object url for the user file is in the following:   
        https://123rf-search-log-dumps.s3.amazonaws.com/output/misc_201907/user_date_register.csv
    
    Convert this into pandas readable format (make sure to use 's3a', not 's3'):
        s3a://123rf-search-log-dumps/output/misc_201907/user_date_register.csv
    
    Alternatively, just open the file in bucket from browser and look for the 'copy path' option 
    from the file 'Overview' tab.
    
    
    Parameters
    ----------
    file_path : str
        The file path for the data file. Data file must be a .csv file in s3 bucket.
        
    Return
    ------
    user : dataframe object (pandas)
        Returns pandas dataframe.
    
    
    """
    register = pd.read_csv(file_path, sep = ";")
    register.date_register = pd.to_datetime(register.date_register)
    return(register)


# In[ ]:





# In[ ]:





# Testing the functions using Search Log data
# ===
# 
# Save the following:
# 1. `xxx_sales_1` as .csv file for first ever transactions done (looks at `purchasetype`==1).
# 2. `xxx_sales_0` as .csv file for repeated transactions done (looks at `purchasetype`==0).
# 3. `xxx_NoSale` as .csv file for non-transaction (basically, all the `user_id` not found in `sales.csv` data).
# 
# where `xxx` in the above refers to the months from May 2018 to Jun 2019 (so for May of 2018, `xxx_sales_1` is `may2018_sales_1`.
# 
# At the end of the dataframe, just before saving to csv file, make sure to add a final feature coulunn `transaction` to the dataframe with the following values:
# 1. `transaction = 1` for first transaction.
# 2. `transaction = 2` for repeated transaction.
# 3.  `transaction = 0` for no transactions.

# In[14]:


df_sales_2019 = sales_data_2019("s3://123rf-search-log-dumps/output/data_pipeline/sales_2019/w1_w48_sales.csv")

#df_sales_2018 = sales_data_2018("s3a://123rf-search-log-dumps/output/sales.csv")


# In[15]:


df_user = user_data("s3a://123rf-search-log-dumps/output/misc_201911/user.csv")


# In[16]:


df_register = register_data("s3://123rf-search-log-dumps/output/misc_201911/member.csv")


# In[17]:


# This is done as testing.
# The files in this location are the weeekly preprocessed level 2 search log files

day_oct = ['07','14','21','28']

df_oct = pd.DataFrame()
for day in day_oct:
    temp = pd.read_csv("s3a://user-hotlist-output/search_pipeline_output/2019/10/"+day+"/fasttext_combined_level_2.csv")
    df_oct = pd.concat([df_oct,temp])

df_oct.SS_T_First = pd.to_datetime(df_oct.SS_T_First)
df_oct.SS_T_Last = pd.to_datetime(df_oct.SS_T_Last)
df_oct.SS_DL_T_First = pd.to_datetime(df_oct.SS_DL_T_First)
df_oct.SS_DL_T_Last = pd.to_datetime(df_oct.SS_DL_T_Last)
df_oct.SS_VL_T_First = pd.to_datetime(df_oct.SS_VL_T_First)
df_oct.SS_VL_T_Last = pd.to_datetime(df_oct.SS_VL_T_Last)


# In[18]:


# This is done as testing.
# The files in this location are the weeekly preprocessed level 2 search log files

day_nov = ['04','11','18','25']

df_nov = pd.DataFrame()
for day in day_nov:
    temp = pd.read_csv("s3a://user-hotlist-output/search_pipeline_output/2019/11/"+day+"/fasttext_combined_level_2.csv")
    df_nov = pd.concat([df_nov,temp])

df_nov.SS_T_First = pd.to_datetime(df_nov.SS_T_First)
df_nov.SS_T_Last = pd.to_datetime(df_nov.SS_T_Last)
df_nov.SS_DL_T_First = pd.to_datetime(df_nov.SS_DL_T_First)
df_nov.SS_DL_T_Last = pd.to_datetime(df_nov.SS_DL_T_Last)
df_nov.SS_VL_T_First = pd.to_datetime(df_nov.SS_VL_T_First)
df_nov.SS_VL_T_Last = pd.to_datetime(df_nov.SS_VL_T_Last)


# # Transaction dataset

# In[19]:


jan_2019 = transaction_data('jan',df_sales_2019,df_user,df_register,2019,1)
feb_2019 = transaction_data('feb',df_sales_2019,df_user,df_register,2019,2)
mar_2019 = transaction_data('mar',df_sales_2019,df_user,df_register,2019,3)
apr_2019 = transaction_data('apr',df_sales_2019,df_user,df_register,2019,4)
may_2019 = transaction_data('may',df_sales_2019,df_user,df_register,2019,5)
jun_2019 = transaction_data('jun',df_sales_2019,df_user,df_register,2019,6)
jul_2019 = transaction_data('jul',df_sales_2019,df_user,df_register,2019,7)
aug_2019 = transaction_data('aug',df_sales_2019,df_user,df_register,2019,8)
sep_2019 = transaction_data('sep',df_sales_2019,df_user,df_register,2019,9)


# In[20]:


oct_2019 = oct_nov_transaction(df_oct,df_sales_2019,df_user,df_register,2019,10)


# In[21]:


nov_2019 = oct_nov_transaction(df_nov,df_sales_2019,df_user,df_register,2019,11)


# In[22]:


trans_data = pd.DataFrame()
trans_data = pd.concat([trans_data,jan_2019], ignore_index=True, sort=False)
trans_data = pd.concat([trans_data,feb_2019], ignore_index=True, sort=False)
trans_data = pd.concat([trans_data,mar_2019], ignore_index=True, sort=False)
trans_data = pd.concat([trans_data,apr_2019], ignore_index=True, sort=False)
trans_data = pd.concat([trans_data,may_2019], ignore_index=True, sort=False)
trans_data = pd.concat([trans_data,jun_2019], ignore_index=True, sort=False)


# In[23]:


SIPtrans_data = pd.DataFrame()
SIPtrans_data = pd.concat([SIPtrans_data,jul_2019], ignore_index=True, sort=False)
SIPtrans_data = pd.concat([SIPtrans_data,aug_2019], ignore_index=True, sort=False)
SIPtrans_data = pd.concat([SIPtrans_data,sep_2019], ignore_index=True, sort=False)
SIPtrans_data = pd.concat([SIPtrans_data,oct_2019], ignore_index=True, sort=False)


# In[ ]:





# In[ ]:





# # NO Transaction data

# In[24]:


jan_no_2019 = NO_transaction_data('jan',df_sales_2019,df_user,2019,1)
feb_no_2019 = NO_transaction_data('feb',df_sales_2019,df_user,2019,2)
mar_no_2019 = NO_transaction_data('mar',df_sales_2019,df_user,2019,3)
apr_no_2019 = NO_transaction_data('apr',df_sales_2019,df_user,2019,4)
may_no_2019 = NO_transaction_data('may',df_sales_2019,df_user,2019,5)
jun_no_2019 = NO_transaction_data('jun',df_sales_2019,df_user,2019,6)
jul_no_2019 = NO_transaction_data('jul',df_sales_2019,df_user,2019,7)
aug_no_2019 = NO_transaction_data('aug',df_sales_2019,df_user,2019,8)
sep_no_2019 = NO_transaction_data('sep',df_sales_2019,df_user,2019,9)


# In[25]:


oct_no_2019 = oct_nov_NO_transaction_data(df_oct,df_sales_2019,df_user,2019,10)


# In[26]:


nov_no_2019 = oct_nov_NO_transaction_data(df_nov,df_sales_2019,df_user,2019,11)


# In[27]:


no_trans_data = pd.DataFrame()
no_trans_data = pd.concat([no_trans_data,jan_no_2019], ignore_index=True, sort=False)
no_trans_data = pd.concat([no_trans_data,feb_no_2019], ignore_index=True, sort=False)
no_trans_data = pd.concat([no_trans_data,mar_no_2019], ignore_index=True, sort=False)
no_trans_data = pd.concat([no_trans_data,apr_no_2019], ignore_index=True, sort=False)
no_trans_data = pd.concat([no_trans_data,may_no_2019], ignore_index=True, sort=False)
no_trans_data = pd.concat([no_trans_data,jun_no_2019], ignore_index=True, sort=False)


# In[28]:


SIPno_trans_data = pd.DataFrame()
SIPno_trans_data = pd.concat([SIPno_trans_data,jul_no_2019], ignore_index=True, sort=False)
SIPno_trans_data = pd.concat([SIPno_trans_data,aug_no_2019], ignore_index=True, sort=False)
SIPno_trans_data = pd.concat([SIPno_trans_data,sep_no_2019], ignore_index=True, sort=False)
SIPno_trans_data = pd.concat([SIPno_trans_data,oct_no_2019], ignore_index=True, sort=False)


# In[ ]:





# # Saving to file

# In[29]:


SIPtrans_data.to_csv("s3a://logo-generator-kj/rish/SIPtrans_data.csv")


# In[30]:


SIPno_trans_data.to_csv("s3a://logo-generator-kj/rish/no_SIPtrans_data.csv")


# In[31]:


nov_2019.to_csv("s3a://logo-generator-kj/rish/nov_trans_data.csv")


# In[32]:


nov_no_2019.to_csv("s3a://logo-generator-kj/rish/nov_notrans_data.csv")


# In[ ]:


# trans_data.to_csv("trans_2019data_auto.csv", index=False)
# no_trans_data.to_csv("no_trans_2019data_auto.csv", index=False)


# In[ ]:


# sep_2019 = transaction_data('sep',df_sales_2019,df_user,df_register,2019,9)
# sep_no_2019 = NO_transaction_data('sep',df_sales_2019,df_user,2019,9)


# In[ ]:


# sep_2019.to_csv("sep_trans_2019data_auto.csv", index=False)
# sep_no_2019.to_csv("sep_no_trans_2019data_auto.csv", index=False)


# In[ ]:





# Probably no need
# ==

# In[ ]:


# jan_2019 = transaction_data('jan',df_sales_2019,df_user,df_register,2019,1)
# feb_2019 = transaction_data('feb',df_sales_2019,df_user,df_register,2019,2)
# mac_2019 = transaction_data('mar',df_sales_2019,df_user,df_register,2019,3)
# apr_2019 = transaction_data('apr',df_sales_2019,df_user,df_register,2019,4)
# may_2019 = transaction_data('may',df_sales_2019,df_user,df_register,2019,5)
# jun_2019 = transaction_data('jun',df_sales_2019,df_user,df_register,2019,6)


# In[ ]:


# trans_data_2019 = pd.DataFrame()
# trans_data_2019 = pd.concat([trans_data_2019,jan_2019], ignore_index=True, sort=False)
# trans_data_2019 = pd.concat([trans_data_2019,feb_2019], ignore_index=True, sort=False)
# trans_data_2019 = pd.concat([trans_data_2019,mac_2019], ignore_index=True, sort=False)
# trans_data_2019 = pd.concat([trans_data_2019,apr_2019], ignore_index=True, sort=False)
# trans_data_2019 = pd.concat([trans_data_2019,may_2019], ignore_index=True, sort=False)
# trans_data_2019 = pd.concat([trans_data_2019,jun_2019], ignore_index=True, sort=False)


# In[ ]:


# jan_no_2019 = NO_transaction_data('jan',df_sales_2019,df_user,2019,1)
# feb_no_2019 = NO_transaction_data('feb',df_sales_2019,df_user,2019,2)
# mar_no_2019 = NO_transaction_data('mar',df_sales_2019,df_user,2019,3)
# apr_no_2019 = NO_transaction_data('apr',df_sales_2019,df_user,2019,4)
# may_no_2019 = NO_transaction_data('may',df_sales_2019,df_user,2019,5)
# jun_no_2019 = NO_transaction_data('jun',df_sales_2019,df_user,2019,6)


# In[ ]:


# no_trans_data_2019 = pd.DataFrame()
# no_trans_data_2019 = pd.concat([no_trans_data_2019,jan_no_2019], ignore_index=True, sort=False)
# no_trans_data_2019 = pd.concat([no_trans_data_2019,feb_no_2019], ignore_index=True, sort=False)
# no_trans_data_2019 = pd.concat([no_trans_data_2019,mar_no_2019], ignore_index=True, sort=False)
# no_trans_data_2019 = pd.concat([no_trans_data_2019,apr_no_2019], ignore_index=True, sort=False)
# no_trans_data_2019 = pd.concat([no_trans_data_2019,may_no_2019], ignore_index=True, sort=False)
# no_trans_data_2019 = pd.concat([no_trans_data_2019,jun_no_2019], ignore_index=True, sort=False)


# In[ ]:


# trans_data_2019.to_csv("trans_data_2019_auto.csv")
# no_trans_data_2019.to_csv("no_trans_2019_data_auto.csv")


# In[ ]:


# sep_2019 = transaction_data('sep',df_sales_2019,df_user,df_register,2019,1)


# In[ ]:


# sep_no_2019 = NO_transaction_data('sep',df_sales_2019,df_user,2019,1)


# In[ ]:


def test_features(ss_may_sale2):
    """
    This function is mainly used for feature engineering preprocessing step, namely to get the
    'browse_diff' (difference between browsing time) and 'first_last_diff' (different between
    first and last browsing session). But before getting these two features, the data has to be
    sorted using .sort_values(['user_id','SS_T_First','browsing_id']) method, according to the
    selected features.
    
    This function also selects only the following features for the machine learning prediction
    (and modeling):
    ['user_id', 'browsing_id', 'SS_TR_T_First','SS_TR_T_Last',\
    'SS_T_First', 'SS_T_Last', 'SS_Page_Count_Sum', 'SS_Page_Count_Mean','SS_Page_Count_Std',\
    'SS_T_Sum_Sum','SS_T_Sum_Std','SS_T_Mean_Sum','SS_T_Mean_Std','SS_T_Std_Sum','SS_T_Std_Std',\
    'SS_DL_Count_Sum','SS_T_Sum_Mean','SS_T_Mean_Mean', 'SS_T_Std_Mean', 'pdate_TFirst_diff',\
    'browse_diff', 'first_last_diff', 'transaction']
    
    
    Parameters
    ----------
    ss_may_sale2 : dataframe object (pandas)
        Pandas dataframe object generated by the search_sale() function.

    
    Returns
    -------
    ss_may_sale1_3 : dataframe object (pandas)
    
    
    """
        
    ss_may_sale1_3 = ss_may_sale2.sort_values(['user_id','SS_T_First','browsing_id'])
    
    ss_may_sale1_3['browse_diff'] = ss_may_sale1_3.SS_T_First.diff()
    ss_may_sale1_3.browse_diff = ss_may_sale1_3.browse_diff.dt.total_seconds()
    
    ss_may_sale1_3['first_last_diff'] = ss_may_sale1_3.SS_T_Last - ss_may_sale1_3.SS_T_First
    ss_may_sale1_3.SS_VL_T_Last = pd.to_datetime(ss_may_sale1_3.SS_VL_T_Last)
    ss_may_sale1_3.SS_VL_T_First = pd.to_datetime(ss_may_sale1_3.SS_VL_T_First)
    ss_may_sale1_3['VL_time_diff'] = ss_may_sale1_3.SS_VL_T_Last - ss_may_sale1_3.SS_VL_T_First
    
    ss_may_sale1_3.first_last_diff = ss_may_sale1_3.first_last_diff.dt.total_seconds()
    ss_may_sale1_3.VL_time_diff = ss_may_sale1_3.VL_time_diff.dt.total_seconds()
    t_mean = ss_may_sale1_3.VL_time_diff.mean()
    ss_may_sale1_3.VL_time_diff.fillna(t_mean, inplace=True)
    
    ss_may_sale1_3 = ss_may_sale1_3.loc[:,['user_id', 'browsing_id','SS_T_First','SS_T_Last',                                            'SS_Page_Count_Sum', 'SS_Page_Count_Mean','SS_Page_Count_Std',                                            'SS_T_Sum_Sum', 'SS_T_Sum_Mean', 'SS_T_Sum_Std',                                            'SS_T_Mean_Sum','SS_T_Mean_Std', 'SS_T_Mean_Mean',                                            'SS_T_Std_Sum', 'SS_T_Std_Std', 'SS_T_Std_Mean',                                            'SS_VL_Count_Sum', 'SS_VL_Count_Mean','SS_VL_Count_Std',                                           'SS_VL_T_Sum_Sum', 'SS_VL_T_Sum_Mean', 'SS_VL_T_Sum_Std',                                            'SS_VL_T_Mean_Sum','SS_VL_T_Mean_Std', 'SS_VL_T_Mean_Mean',                                            'SS_VL_T_Std_Sum', 'SS_VL_T_Std_Std', 'SS_VL_T_Std_Mean',                                            'SS_DL_Count_Sum','VL_time_diff',                                           'browse_diff', 'first_last_diff']].copy()
    ss_may_sale1_3.reset_index(inplace=True)
    ss_may_sale1_3.drop(['index'], axis=1, inplace=True)
    
    first_repeat = ss_may_sale1_3[~(ss_may_sale1_3.user_id.duplicated(keep='first'))].index.to_list()
    # print(ss_may_sale1_3.iloc[1,-2] - ss_may_sale1_3.iloc[1,-2])
    ss_may_sale1_3.iloc[0,-1] = ss_may_sale1_3.iloc[1,-1] - ss_may_sale1_3.iloc[1,-1]
    ss_may_sale1_3.iloc[first_repeat,-2] =  ss_may_sale1_3.iloc[0,-1]

    return(ss_may_sale1_3)


# In[ ]:


sep_2019 = load_data('sep')
test_sep2019 = test_features(sep_2019)


# In[ ]:


test_sep2019.to_csv("s3a://logo-generator-kj/rish/sep2019_test.csv", index=False)


# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:


sep2019_w1 = sep_2019[sep_2019.SS_T_First.dt.week == 35]


# In[ ]:


sep2019_w2 = sep_2019[sep_2019.SS_T_First.dt.week == 36]


# In[ ]:


sep2019_w3 = sep_2019[sep_2019.SS_T_First.dt.week == 37]


# In[ ]:


sep2019_w4 = sep_2019[sep_2019.SS_T_First.dt.week == 38]


# In[ ]:


sep2019_w2.to_csv("s3a://logo-generator-kj/rish/sep2019_w2.csv", index=False)
sep2019_w3.to_csv("s3a://logo-generator-kj/rish/sep2019_w3.csv", index=False)
sep2019_w4.to_csv("s3a://logo-generator-kj/rish/sep2019_w4.csv", index=False)


# In[ ]:





# In[ ]:





# In[ ]:




